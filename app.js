const fs = require("fs");
const _ = require("lodash");
const yargs = require("yargs");
const newNote = require("./newNote.js");

const numberOptions = {
    describe: "number",
    demand: true,
    alias: 'n'
};
const lunchOptions = {
    describe: "lunch",
    demand: true,
    alias: 'l'
}
const priceOptions = {
    describe: "price",
    demand: true,
    alias: 'p'
}

var command = process.argv[2];
var argv = yargs
.command("add", "Add a new order", {
    number: numberOptions,
    lunch: lunchOptions,
    price: priceOptions
})
.command("list", "list all orders")
.command("read", "read a order",{
    number: numberOptions,
    lunch: lunchOptions
})
.command("remove", "remove a order", {
    number: numberOptions,
    lunch: lunchOptions
})
.command("price", "Total cost of the meals")
.help()
.argv;


if(command === "add"){
   var order = newNote.addNote(argv.number,argv.lunch, argv.price);
   if(order){
       console.log("order created");
       console.log("---");
       console.log(`Number: ${order.number}`);
       console.log(`Lunch: ${order.lunch}`);
       console.log(`Price: ${order.price}`);
   }
   else{
       console.log("order taken");
   }
}
else if(command === "list"){
   var allOrders = newNote.getAll(argv.number);
   var total = 0;
   for(var i =0; i<allOrders.length;i++){
        total += allOrders[i].number;
       
   }
   console.log(`we have ${total} meals to make`);
   allOrders.forEach((order) => { 
         console.log("---");
         console.log(`Number: ${order.number}`);
         console.log(`Lunch: ${order.lunch}`);
         console.log(`Price: ${order.price}`)
   });
}
else if (command === "read"){
   var order =  newNote.getNote(argv.number, argv.lunch);
    if(order){
        console.log("order found");
        console.log("---");
        console.log(`Number: ${order.number}`);
        console.log(`Lunch: ${order.lunch}`);
        console.log(`Price: ${order.price}`);
    }else{
        console.log("order not found");
    }
}
else if(command === "remove"){
   var noteRemoved = newNote.removeNote(argv.number, argv.lunch)
   var message = noteRemoved ? "order was removed": "order not found";
   console.log(message);
}

else if( command === "price"){
  var pricee = newNote.priceAll(argv.price);
  var sum = 0;
  for(var i = 0; i<pricee.length; i++){
      sum += pricee[i].price;
  }
  console.log(`Price: ${sum}`);
}else{
    console.log("not recognized");
}



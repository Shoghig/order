const fs = require("fs");

var fetchOrders = () => {
      var orderString = fs.readFileSync("orders-data.json");
      return JSON.parse(orderString);
};

var saveOrders = (orders) => {
    fs.writeFileSync("orders-data.json", JSON.stringify(orders));
}
//---------------------------------
var addNote= (number,lunch, price) => {
    var orders = fetchOrders();
    var order = {
        number,
        lunch,
        price
    };

    var duplicateOrder = orders.filter((order) => order.number === number && order.lunch === lunch);
    if(duplicateOrder.length === 0){
        orders.push(order);
        saveOrders(orders);
        return order;
    }
};
var getAll = (number) => {
    return fetchOrders();
}

var getNote = (number, lunch) => {
   var orders = fetchOrders();
    var filteredOrders = orders.filter((order_) => order_.number === number && order_.lunch === lunch);
    return filteredOrders[0];
}

var removeNote = (number, lunch) => {
   var orders = fetchOrders();
   var filteredOrders = orders.filter((order) => order.number !== number && order.lunch !== lunch);
   saveOrders(filteredOrders);

   return orders.length !== filteredOrders.length;
};

var priceAll = (price) => {
  return fetchOrders();
}

module.exports = {
    addNote,
    getAll,
    getNote,
    removeNote,
    priceAll
}